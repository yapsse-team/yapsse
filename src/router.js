import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Search from './views/Search.vue'
import Engineer from './views/Engineer.vue'
import Sources from './views/Sources.vue'
import Log from './views/Log.vue'
import Code from './views/Code.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/engineer',
      name: 'engineer',
      component: Engineer
    },
    {
      path: '/sources',
      name: 'sources',
      component: Sources
    },
    {
      path: '/log',
      name: 'log',
      component: Log
    },
    {
      path: '/code',
      name: 'code',
      component: Code
    }
  ]
})
