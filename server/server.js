const app = require('./app')

app.listen(app.get('port'), () => {
  console.log('Yapsse is listening on port ' + app.get('port'))
  console.log(`Open http://localhost:${app.get('port')}/`)
})
