require('dotenv').config()
const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const api = require('./api')

const app = express()

app.set('port', process.env.PORT)
app.set('env', process.env.ENV)

app.use(express.static(path.join(__dirname, '../dist')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/api', api)

// app.get('/', () => {
//   res.render('index.html')
// })

// 404
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(new Error('Not Found'))
})

// Error-Handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.send(err.message)
})

module.exports = app
