import json

null_edge = "#empty_conn"
static = 0.9
by_same = 0.8
by_another = 0.7

weights = {}
norm = {} # dict <id, normed_id>
marked = {}
edges = [] #adjacency list
nodes_id = {} #dict <id, label>

def dfs(id, w, prev):
    v = norm[id]
    #print(str(nodes_id[id]) + ' ' + str(w))
    weights[id] = weights[id] + w
    if marked[id] == 1:
        return
    marked[id] = 1
    for edge in edges[v]:
        coeff = static
        if prev != null_edge:
            if edge['label'] != 'synonym':
                coeff = coeff * (by_same if prev == edge['label'] else by_another)
        dfs(edge['to'], w * coeff, edge['label'])

def weighter(sem_web, concepts):
    web = json.loads(sem_web.lower())
    
    nodes_size = len(web['nodes'])
    for i in range(nodes_size):
        edges.append([])
    nodes = [web['nodes'][i]['id'] for i in range(nodes_size)] #nodes by id
    for node in nodes:
        weights[node] = 0
        marked[node] = 0
    for node in web['nodes']:
        nodes_id[node['id']] = node['label']
    conc_id = [] #dict <concept_label, id>
    bad_conc = [] #list of bad concepts
    for conc in concepts:
        flag = 0
        for node in nodes:
            if conc.lower() == nodes_id[node]:
                conc_id.append((conc.lower(), node))
                flag = 1
                break
        if flag == 0:
            bad_conc.append(conc.lower())
        
    
    num = 0
    for node in nodes:
        norm[node] = num
        num = num + 1
        
    for edge in web['edges']:
        v = norm[edge['from']]
        u = norm[edge['to']]
        if edge['label'] == 'is_a' or edge['label'] == 'a_part_of' or edge['label'] == 'synonym':
            tmp = dict(edge)
            tmp['from'] = edge['to']
            tmp['to'] = edge['from']
            edges[u].append(tmp)
        else:
            edges[v].append(edge)
    
    for key, val in conc_id:
        dfs(val, 1.0, null_edge)
    res = [(nodes_id[x], weights[x]) for x in nodes if weights[x] > 0]
    res.sort(reverse=True, key = lambda x:x[1])
    result = {}
    for x in nodes:
        if weights[x] != 0.0 and len(nodes_id[x]) != 0 and nodes_id[x][0] != '#':
            result[nodes_id[x].lower()] = weights[x]
    return result

### concept_weight_dict = weighter(json_sem_web, concept_label_list)