from pymystem3 import Mystem

def parse():
    stopwords = ['?', 'каков', 'какой', 'сколько', '\n', ' ', 'иметь', 'быть', 'ли', 'как', 'за', 'необходимо', 'жить', 'в', 'использовать', 'у', 'где', 'ходить']
    query = input()
    m = Mystem()
    result_list = []
    lemm = m.lemmatize(query)
    for w in stopwords:
        if w in lemm:
            lemm = list(filter(lambda x: x!=w, lemm))
    print(lemm)
    txt = ' '.join(lemm)
    if 'ухаживать' in txt:
        if 'ухаживать коготь' in txt:
            result_list.append('Уход за когтями')
            lemm.remove('ухаживать')
            lemm.remove('коготь')
        else:
            result_list.append('Уход')
            lemm.remove('ухаживать')

    if 'продолжительность жизнь' in txt:
        result_list.append('Продолжительность жизни')
        lemm.remove('продолжительность')
        lemm.remove('жизнь')

    if 'часто есть' in txt:
        result_list.append('Частота питания')
        lemm.remove('часто')
        lemm.remove('есть')

    if 'мыть' in txt:
        result_list.append('Мытье')
        lemm.remove('мыть')

    if 'вычесывать' in txt:
        result_list.append('Вычесывание')
        lemm.remove('вычесывать')

    if 'коготь' in txt:
        if 'стричь коготь' in txt:
            result_list.append('Стрижка когтей')
            lemm.remove('стричь')
            lemm.remove('коготь')
        else:
            result_list.append('Когти')
            lemm.remove('коготь')

    if 'масса корм' in txt:
        result_list.append('Масса корма')
        lemm.remove('масса')
        lemm.remove('корм')

    if 'вид корм' in txt:
        result_list.append('Вид корма')
        lemm.remove('вид')
        lemm.remove('корм')

    if 'питаться' in txt:
        result_list.append('Питание')
        lemm.remove('питаться')

    if 'домашний животное' in txt:
        result_list.append('Домашнее животное')
        lemm.remove('домашний')
        lemm.remove('животное')

    if ' г' in txt:
        jn(lemm, 'г')

    if ' кг' in txt:
        jn(lemm, 'кг')

    if 'раз день' in txt:
        i = lemm.index('день')
        lemm[i - 2] = lemm[i - 2] + ' ' + lemm[i - 1] + ' в ' + lemm[i]
        lemm.remove(lemm[i])

    result_list.extend(lemm)
    return result_list

def jn(lemm, str):
    i = lemm.index(str)
    lemm[i - 1] = lemm[i - 1] + ' ' + lemm[i]
    lemm.remove(lemm[i])

print(parse())