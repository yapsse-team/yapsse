# -*- coding: utf-8 -*-
file_log = open('C:/Files/Projects/yapsse/server/log.txt', 'w')



from pymystem3 import Mystem
from bs4 import BeautifulSoup
import re
import Stemmer #if problems - download cython
import os
import json
import string

"""weight of 1 html page
file        - html file - STRING, not path
word_list   - dictionary, key: string as semweb's node, value: weight of node"""

def count_fileweight(file, word_list):
    text = from_html_to_text(file)
    word_list = stem_dist(word_list)
    weight = 0
    count_dict = fill_count_dict(word_list)
    #m = Mystem()
    #lemm = m.lemmatize(text)

    lemm = stem(text)
    #lemm = list(filter(lambda x: x!='\n' and x!=' ', lemm))
    for word in lemm:
        if word in word_list:
            #weight += word_list[word]
            count_dict[word] += 1
    
    for key, count in count_dict.items():
         weight += word_list[key] * (count **(1/4))
    return weight

def fill_count_dict(word_list):
    result = dict(zip(word_list.keys(), [0] * len(word_list)))
    return result

def stem(text):
    m = Stemmer.Stemmer('russian')
    lst = text.lower().translate(str.maketrans("", "", string.punctuation)).split()
    return m.stemWords(lst)


def stem_dist(word_list):
    #m = Mystem()
    new_wl = {}
    for key, val in word_list.items():
        #wd = m.lemmatize(key)
        wd = stem(key)
        #wd = list(filter(lambda x: x != '\n' and x != ' ', wd))
        #if (len(wd) > 1):
        #    new_wl[key] = [val, wd]
        if (wd[0] in new_wl):
            new_wl[wd[0]] += val
        else:
            new_wl[wd[0]] = val
    return new_wl


def from_html_to_text(html):
    soup = BeautifulSoup(html, features = 'html.parser')
    text = ""
    for meta in soup.find_all('meta'):
        if (meta.get('content') != None):
            text += meta.get('content') + '\n'
    text += soup.get_text()
    text = re.sub(r'\n{2,}', '\n', text)
    return text

def count_weights(files, nodes):
    sorted_file_lst = {}
    for filename in files:
        file = open(os.path.join('C:/Files/Projects/yapsse/server/web_pages', filename), 'r', encoding='utf-8')
        text = file.read()
        sorted_file_lst[filename] = count_fileweight(text, nodes)
        file.close()
    result = sorted(sorted_file_lst.items(), key=lambda x: x[1], reverse=True)
    print(result, file=file_log)
    return result

def parse(query):
    stopwords = ['?', 'каков', 'какой', 'сколько', '\n', ' ', 'как', 'где', 'что', 'кто']
    #print(stopwords, file=file_log)
    m = Mystem()
    lemm = m.lemmatize(query)
    for w in stopwords:
        if w in lemm:
            lemm = list(filter(lambda x: x!=w, lemm))
    return lemm

def jn(lemm, str):
    i = lemm.index(str)
    lemm[i - 1] = lemm[i - 1] + ' ' + lemm[i]
    lemm.remove(lemm[i])

import json

null_edge = "#empty_conn"
static = 0.7
by_same = 0.5
by_another = 0.4

weights = {}
norm = {}  # dict <id, normed_id>
marked = {}
edges = []  # adjacency list
nodes_id = {}  # dict <id, label>


def dfs(id, w, prev, p):
    v = norm[id]
    # print(str(nodes_id[id]) + ' ' + str(w))
    weights[id] = weights[id] + w
    if marked[id] == 1:
        return
    marked[id] = 1
    for edge in edges[v]:
        coeff = static
        if prev != null_edge:
            if edge['label'] != 'synonym':
                coeff = coeff * (by_same if prev == edge['label'] else by_another)
        if p != edge['to']:
            dfs(edge['to'], w * coeff, edge['label'], id)


def weighter(sem_web, concepts):

    web = json.loads(sem_web.lower())

    nodes_size = len(web['nodes'])
    for i in range(nodes_size):
        edges.append([])
    nodes = [web['nodes'][i]['id'] for i in range(nodes_size)]  # nodes by id

    for node in nodes:
        weights[node] = 0
        marked[node] = 0
    for node in web['nodes']:
        nodes_id[node['id']] = node['label'].lower()
    #print('!!!', file=file_log)
    #print(concepts, file=file_log)

    conc_id = []  # dict <concept_label, id>
    bad_conc = []  # list of bad concepts
    #print(concepts, file=file_log)
    for conc in concepts:
        flag = 0
        for node in nodes:
            if conc.lower() == nodes_id[node]:
                conc_id.append((conc.lower(), node))
                flag = 1
                break
        if flag == 0:
            bad_conc.append(conc.lower())
    #print(bad_conc, file=file_log)
    num = 0
    for node in nodes:
        norm[node] = num
        num = num + 1

    for edge in web['edges']:
        v = norm[edge['from']]
        u = norm[edge['to']]
        if edge['label'] == 'is_a' or edge['label'] == 'a_part_of' or edge['label'] == 'synonym':
            tmp = dict(edge)
            tmp['from'] = edge['to']
            tmp['to'] = edge['from']
            edges[u].append(tmp)
        edges[v].append(edge)

    #print(conc_id, file=file_log)
    for key, val in conc_id:
        dfs(val, 1.0, null_edge, -1)
    res = [(nodes_id[x], weights[x]) for x in nodes if weights[x] > 0 and nodes_id[x] != ' ' and len(nodes_id[x]) > 0]
    res.sort(reverse=True, key=lambda x: x[1])
    result = {}
    for x in nodes:
        if weights[x] != 0.0 and len(nodes_id[x]) != 0 and nodes_id[x][0] != '#' and nodes_id[x] != ' ':
            result[nodes_id[x].lower()] = weights[x]
    print(res, file=file_log)
    return result
    
def to_json(filelist):
    json_list = []
    print(filelist, file=file_log)
    for file in filelist:
        json_list.append({'filename': file[0], 'weight': file[1]})
    
    #print(json.dumps(json_list), file=file_log)
    return json.dumps(json_list)

def run():
    json_file = input().encode('cp1251').decode('utf8')
    f = json.loads(json_file)
    #print(f, file=file_log)
    print(to_json(count_weights(f['files'], weighter(json.dumps(f['semantic_web']['data']), parse(f['query'])))))

run()