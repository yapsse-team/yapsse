const Datastore = require('nedb-promise')
const path = require('path')

const Config = new Datastore({
  filename: path.join(__dirname, './db/config.db'),
  autoload: true
})

const Sites = new Datastore({
  filename: path.join(__dirname, './db/sites.db'),
  autoload: true
})

const SemanticNetwork = new Datastore({
  filename: path.join(__dirname, './db/semantic_network.db'),
  autoload: true
})

const Main = new Datastore({
  filename: path.join(__dirname, './db/main.db'),
  autoload: true
})

const DB = {
  Config, Sites, SemanticNetwork, Main
}

module.exports = DB
