const path = require('path')
const fs = require('fs')
const express = require('express')
const DB = require('./db')
const { spawn } = require('child_process')
const iconv = require('iconv-lite')
const multer  = require('multer')

const router = express.Router()
const storage = multer.diskStorage({
  destination(req, file, callback){
		callback(null, path.join(__dirname, 'web_pages/'))
	},
	filename(req, file, callback){
		callback(null, file.originalname);
	}
})
const upload = multer({ storage })

router.get('/test', (req, res) => {
  DB.Main.insert({
    name: 'test'
  }, (err, data) => {
    if (err) {
      throw new Error(err)
    }
    res.json(data)
  })
})

router.post('/semantic-network', async (req, res) => {
  const count = await DB.SemanticNetwork.count({})
  if (!count) {
    await DB.SemanticNetwork.insert({ id: 1, data: req.body })
  } else {
    await DB.SemanticNetwork.update({ id: 1 }, { id: 1, data: req.body })
  }
  res.send('ok')
})

router.get('/semantic-network', async (req, res) => {
  const result = await DB.SemanticNetwork.findOne({ id: 1 })
  if (result) {
    res.json(result.data)
  } else {
    res.json({})
  }
})

router.get('/search', async (req, res) => {
  if (!req.query.query) {
    res.json([])
    return
  }

  const files = fs.readdirSync(path.join(__dirname, 'web_pages'))
  const semanticWeb = await DB.SemanticNetwork.findOne({ id: 1 })
  const query = req.query.query

  const python = spawn('py', [path.join(__dirname, './weighter/query_results.py')])

  python.stdin.write(JSON.stringify({ files, query, semantic_web: semanticWeb }) + '\n')
  python.stdin.end()

  let result = ""
  let error = null

  python.stdout.on('data', (data) => {
    result += data.toString()
  })

  python.stdout.on('end', (data) => {
    if (!error) {
      res.json(JSON.parse(result))
    } else {
      res.status(400).send(error)
    }
    
  })

  python.stderr.on('data', (data) => {
    error += data.toString()
  })
})

router.get('/web-pages', async (req, res) => {
  const files = fs.readdirSync(path.join(__dirname, 'web_pages'))
  res.json(files)
})

router.post('/remove-web-page', async (req, res) => {
  fs.unlinkSync(path.join(__dirname, './web_pages/' + req.body.filename))
  res.end()
})

router.use('/web_pages', express.static(path.join(__dirname, './web_pages')))

router.get('/log', async (req, res) => {
  const log = fs.readFileSync(path.join(__dirname, './log.txt'))
  res.send(iconv.encode(iconv.decode(log, 'cp1251'), 'utf8').toString())
})

router.get('/get-code', async (req, res) => {
  const code = fs.readFileSync(path.join(__dirname, './weighter/query_results.py'))
  res.send(code)
})

router.post('/update-code', async (req, res) => {
  fs.writeFileSync(path.join(__dirname, './weighter/query_results.py'), req.body.code)
  res.send('ok')
})

router.post('/upload', upload.single('file'), (req, res, next) => {
  res.send('ok')
});

module.exports = router
